<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>تبدیلگر</title>
    @vite(['resources/js/app.js'])
</head>
<body>
<div id="app">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card mt-5">
                    <div class="card-header">تبدیلگر</div>
                    <div class="card-body">
                        @if(session('formated_url'))
                            <div class="alert alert-success">
                                <a href="{{ session('formated_url') }}" class="btn btn-outline-success">دانلود</a>
                            </div>
                        @endif
                        @include('Home.components.video-upload')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
