<form method="post" action="{{ route('upload_video') }}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="#video">فایل خود را انتخاب کنید</label>
        <input type="file"
               name="video"
               id="video"
               class="form-control @error('video') is-invalid @enderror"
               accept="video/*"
        >
        @error('video')
        <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="video_format">فرمت</label>
        <select class="form-control" name="video_format" id="video_format">
            <option value="mp4">mp4</option>
            <option value="avi">avi</option>
            <option value="mkv">mkv</option>
        </select>
    </div>

    <div class="d-grid gap-2 mt-2">
        <button type="submit" class="btn btn-success">اپلود</button>
    </div>
</form>
