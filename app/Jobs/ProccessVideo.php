<?php

namespace App\Jobs;

use App\Models\Video;
use Carbon\Carbon;
use FFMpeg\Format\Video\X264;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;

class ProccessVideo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private Video $video;
    private $video_format;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 3600;

    public function __construct(Video $video,$video_format)
    {
        //
        $this->video = $video;
        $this->video_format = $video_format;
        $this->handle();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        set_time_limit(7200);
        $this->format($this->video,$this->video_format);
    }

    public function format(Video $video,$format)
    {
        $video->converted_at = Carbon::now()->format('Y-m-d');
        $source = 'videos/format/' . $video->converted_at . '-' . $video->id . '.' . $format;
        FFMpeg::fromDisk($video->disk)
            ->open($video->path)
            ->export()
            ->inFormat(new X264('libmp3lame' , 'libx264'))
            ->toDisk($video->disk)
            ->save($source);
        $video->url = Storage::disk($video->disk)->url($source);
        if($video->save()) {
            return true;
        }
    }
}
