<?php

namespace App\Http\Controllers;

use App\Jobs\ProccessVideo;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    public function index()
    {
        return view('Home.Index');
    }

    public function upload(Request $request)
    {
        ini_set('max_execution_time', '600');
        ini_set('memory_limit', '3072M');
        // Validation
        $request->validate([
            'video' => 'required|mimetypes:video/mp4,video/avi,video/mkv|max:100000',
            'video_format' => 'required|string'
        ]);

        // File name
        $name = now()->format('Y-m-d') . $request->file('video')->getClientOriginalName();

        // Create New Object From Video Class
        $video = new Video();

        // upload video and save it to path
        $video->path = Storage::disk('public')->putFileAs('videos', $request->file('video'), $name);

        // Video Saved Disk
        $video->disk = 'public';

        // Video Default Format
        $video->video_default_format = $request->file('video')->getClientOriginalExtension();

        if ($video->save()) {
            ProccessVideo::dispatch($video, $request->video_format);
            return back()->with('formated_url', $video->url);
        }


    }


}
